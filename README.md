# Local Manifests LineageLand

## adding local_manifests

`cd .repo`

`git clone https://gitlab.com/87cf962b35146c21d0dd7f84594662b6/los17.1/local-manifests-lineageland -b lineage-17.1 local_manifests`

`cd ..`

`repo sync -c -j$(nproc --all) --force-sync --no-clone-bundle --current-branch --optimized-fetch --prune -q`

## updating local_manifests

`cd .repo/local_manifests`

`git pull`

`cd ../..`
